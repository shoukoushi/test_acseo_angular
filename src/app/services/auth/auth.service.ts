import { Injectable } from '@angular/core';
import { User } from 'src/app/classes/User';
import { UserLiteral } from 'src/app/Interfaces/UserInterface';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {
  constructor(private router: Router) {}
  readonly base_url: string = 'http://localhost:9000/login';

  // Temps que l'on ne reload pas la page on restera connecter
  public json_user?: UserLiteral = null;

  // Requete ajax pour récupérer l'user
  getUser(admin: User): Promise<UserLiteral> {

    if (!this.json_user) {
      const myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');
      const options: RequestInit = {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify( admin )
      };
      const response_fetched: Promise<Response> =
        fetch(this.base_url, options);

      const json_fetched: Promise<UserLiteral> =
        response_fetched.then( response => response.json());

      json_fetched.then((json) => {
        if (!json.error) {
          this.json_user = json;
        }
      });
      return json_fetched;
    } else {
      return Promise.resolve(this.json_user);
    }
  }

  // Guard de vérification de connection
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    console.log(this.json_user);
    if (this.json_user === null) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

}
