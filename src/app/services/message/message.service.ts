import { Injectable } from '@angular/core';
import { Contact } from 'src/app/classes/Contact';
import { ContactLiteral } from 'src/app/Interfaces/ContactInterface';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor() { }

  readonly base_url: string = 'http://localhost:9000/contact';

  // Requete Ajax qui permet d'ajouter un message
  newMessage(message: Contact): Promise<ContactLiteral> {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    const options: RequestInit = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify( message )
    };
    const response_fetched: Promise<Response> =
      fetch(this.base_url, options);

    const json_fetched: Promise<ContactLiteral> =
      response_fetched.then( response => response.json());

    return json_fetched;
  }

  // Requete ajax qui permet de récupérer tout les messages
  allMessage(): Promise<ContactLiteral[]> {
    const response_fetched: Promise<Response> =
      fetch(this.base_url + '/messages');

    const json_fetched: Promise<ContactLiteral[]> =
      response_fetched.then( response => response.json());

    return json_fetched;
  }

  // Requete ajax qui permet de préciser si que l'on vient de check le message
  messageRead(read: number, id: number): Promise<ContactLiteral> {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    const options: RequestInit = {
      method: 'PATCH',
      headers: myHeaders,
      body: JSON.stringify( {'read': read} )
    };
    const response_fetched: Promise<Response> =
      fetch(this.base_url + '/' + id, options);

    const json_fetched: Promise<ContactLiteral> =
      response_fetched.then( response => response.json());

    return json_fetched;
  }
}
