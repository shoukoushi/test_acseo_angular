import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message/message.service';
import { Contact } from 'src/app/classes/Contact';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {
  public messages: Contact[] = [];
  constructor(private messageService: MessageService) { }

  ngOnInit() {
    // Récupere tout les message pour les afficher
    this.messageService.allMessage().then(json => {
      this.messages = json.map(message => new Contact(message));
    });
  }

}
