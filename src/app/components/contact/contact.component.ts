import { Component, OnInit } from '@angular/core';
import { Contact } from '../../classes/Contact';
import { MessageService } from '../../services/message/message.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public contact: Contact = new Contact;
  public message: string;
  constructor(private messageService: MessageService) { }

  ngOnInit() {
  }

  addMessage() {
    // Envoie du message
    this
      .messageService
      .newMessage(this.contact)
      .then(json => {
        this.message = json.great;
      });
  }
}
