import { Component, OnInit } from '@angular/core';
import { User } from '../../classes/User';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public user: User = new User;
  public error: string;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }
  login() {
    // Vérification de l'user pour la connection
    this.authService.getUser(this.user).then(json => {
      if (!json.error) {
        this.router.navigate(['admin']);
      } else {
        this.error = json.error;
      }
    });
  }
}
