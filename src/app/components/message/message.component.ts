import { Component, Input } from '@angular/core';
import { Contact } from 'src/app/classes/Contact';
import { MessageService } from 'src/app/services/message/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent {

  @Input() public message: Contact;
  public great: string;

  constructor(private messageService: MessageService) {

  }

  messageChecked(event) {
    const target = event.target as HTMLInputElement;

    // Permet de check le message
    if (target.checked === true ) {
      this
        .messageService
        .messageRead(1, this.message.id)
        .then(json => {
          this.great = json.great;
      });
    } else {
      this
        .messageService
        .messageRead(0, this.message.id)
        .then(json => {
          this.great = json.great;
      });
    }
  }
}
