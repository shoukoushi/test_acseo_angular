import { ContactLiteral } from '../Interfaces/ContactInterface';

export class Contact {
    public id?: number;
    public name: string;
    public email: string;
    public question: string;
    public read?: number;

    constructor(contactLiteral?: ContactLiteral) {
        if (contactLiteral) {
            this.id = contactLiteral.id;
            this.name = contactLiteral.name;
            this.email = contactLiteral.email;
            this.question = contactLiteral.question;
            this.read = contactLiteral.read;
        }
    }
}
