export interface UserLiteral {
    pseudo: string;
    password: string;
    error?: string;
}
