export interface ContactLiteral {
    id: number;
    name: string;
    email: string;
    question: string;
    read?: number;
    great?: string;
}
